mod =  angular.module 'roaApp.services', ['ngResource']

mod.factory 'Profile', [
  '$resource', ($resource) ->
    $resource '/profile/profile.json', {},
      query:
        method: 'GET',
        transformResponse: (data) -> JSON.parse(data, true).profile
]

mod.factory 'Birth', [
  '$resource', ($resource) ->
    $resource '/roa/birth/:birthId.json', {birthId: '@id'},
        query:
          method:'GET',
          url: '/roa/birth/births.json'
          cache: false,
          params:
            birthId: 'births'
            isArray: false
]

mod.factory 'Stat', [
  '$resource', ($resource) ->
    $resource '/roa/stat/analgesia.csv', {},
      query:
        method:'GET',
        cache: false,
        transformResponse: (response) ->
          header: _.first(response.split("\n")).split(",")
          data: _.map(_.rest(response.split("\n")), (l) -> l.split(","))
]

mod.factory 'Utils', [
  () ->
    idToName: (id, arr) ->
      r = _.first (_.filter arr, (o) -> o.id == id)
      if r then r.name else 'key #{id} not found'
]
