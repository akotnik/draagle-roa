# don't call with module('roaApp', []) because this constructs new module
mod = angular.module('roaApp')

mod.constant 'MAX_ENTRY_AGE', (1000 * 3600 * 24) * 30

mod.constant  'BIRTH_MASTER',
    'analgesias': []
    'apgar_1': 0
    'apgar_5': 0
    'asa': 0
    'birth_date': new Date().toISOString().substr(0,10)
    'birth_seq': 1
    'birth_sort': null
    'created': new Date().toISOString()
    'doctor_id': null
    'fetus_count': 1
    'fetus_position': 'GL'
    'hospital': null
    'patient_born': null
    'patient_preparation': []
    'seq_in_year': null
    'user_id': null

mod.constant 'DOMAIN':
  'HOSPITAL': [
    {'name': 'Brežice', 'id': 'BR'}
    {'name': 'Celje', 'id': 'CE'}
    {'name': 'Izola', 'id': 'IZ'}
    {'name': 'Jesenice', 'id': 'JS'}
    {'name': 'Kranj', 'id': 'KR'}
    {'name': 'Ljubljana', 'id': 'LJ'}
    {'name': 'Maribor', 'id': 'MB'}
    {'name': 'Murska Sobota', 'id': 'MS'}
    {'name': 'Novo mesto', 'id': 'NM'}
    {'name': 'Postojna', 'id': 'PO'}
    {'name': 'Ptuj', 'id': 'PT'}
    {'name': 'Slovenj Gradec', 'id': 'SG'}
    {'name': 'Trbovlje', 'id': 'TB'}
    {'name': 'Šempeter pri Novi Gorici', 'id': 'SP'}
    {'name': 'Na domu', 'id': 'DM'}
    {'name': 'Drugje', 'id': 'OT'}]

  'BIRTH_SORT': [
    {'name': 'vaginalni', 'id': 'V'}
    {'name': 'vaginalni + epizitomija', 'id': 'VEP'}
    {'name': 'vaginalni + vakuum kiwi', 'id': 'VVK'}
    {'name': 'vaginalni + vakuum klasični', 'id': 'VKS'}
    {'name': 'vaginalni + kleščni', 'id': 'VKL'}
    {'name': 'carski rez + splošna anestezija', 'id': 'CSA'}
    {'name': 'carski rez + spinalna', 'id': 'CSP'}
    {'name': 'carski rez + epiduralna', 'id': 'CEP'}
    {'name': 'carski rez + kombinirana (CSE)', 'id': 'CKO'}]

  'FETAL_POSITION': [
    {'name': 'glavična', 'id': 'GL'}
    {'name': 'medenična', 'id': 'MD'}
    {'name': 'prečna', 'id': 'PR'}]

  'PATIENT_PREPARATION': [
    {'name': 'akupunktura', 'id': 'AKU'}
    {'name': 'hipnoza', 'id': 'HIP'}
    {'name': 'joga', 'id': 'JOG'}
    {'name': 'drugo', 'id': 'DRG'}]

  'ANALGESIA': [
    {'name': 'epiduralna', 'id': 'EPD'}
    {'name': 'spinalna', 'id': 'SPN'}
    {'name': 'cse', 'id': 'CSE'}
    {'name': 'paracervikalni blok', 'id': 'PCB'}
    {'name': 'tens', 'id': 'TNS'}
    {'name': 'regionalno drugo', 'id': 'RED'}
    {'name': 'remifentanil', 'id': 'REM'}
    {'name': 'fentanil', 'id': 'FNT'}
    {'name': 'iv drugo', 'id': 'IVD'}
    {'name': 'inhalacijsko oksidul', 'id': 'IOK'}
    {'name': 'inhalacijsko sevofluran', 'id': 'ISV'}
    {'name': 'inhalacijsko drugo', 'id': 'INH'}]

  'COMPLICATION': [
    {'name': 'hipotenzija', 'id': '1', 'group': 'Zaplet pri porodnici', deprecated: true}
    {'name': 'hipotenzija (-20%) brez vazopresorjev', 'id': '20', 'group': 'Zaplet pri porodnici'}
    {'name': 'hipotenzija (-20%) uporaba vazopresorjev', 'id': '21', 'group': 'Zaplet pri porodnici'}
    {'name': 'bradikardija (< 50/min)', 'id': '2', 'group': 'Zaplet pri porodnici'}
    {'name': 'hipoksemija (< 90%)', 'id': '3', 'group': 'Zaplet pri porodnici'}
    {'name': 'motnje zavesti', 'id': '4', 'group': 'Zaplet pri porodnici'}
    {'name': 'smrt porodnice', 'id': '5', 'group': 'Zaplet pri porodnici'}
    {'name': 'slabost', 'id': '6', 'group': 'Zaplet pri porodnici'}
    {'name': 'bruhanje', 'id': '7', 'group': 'Zaplet pri porodnici'}
    {'name': 'srbečica', 'id': '8', 'group': 'Zaplet pri porodnici'}
    {'name': 'nezadovoljiva analgezija', 'id': '9', 'group': 'Zaplet pri porodnici'}
    {'name': 'povišana telesna temperatura', 'id': '22', 'group': 'Zaplet pri porodnici'}
    {'name': 'prekomerna sedacija', 'id': '23', 'group': 'Zaplet pri porodnici'}
    {'name': 'motnje srčnega ritma', 'id': '24', 'group': 'Zaplet pri porodnici'}
    {'name': 'reanimacija porodnice', 'id': '25', 'group': 'Zaplet pri porodnici'}
    {'name': 'drugo - glej opis', 'id': '26', 'group': 'Zaplet pri porodnici'}

    {'name': 'nevrološki izpadi', 'id': '14', 'group': 'Zaplet regionalne analgezije', deprecated: true}
    {'name': 'intravnesko vbrizgavanje lokalnega anestetika', 'id': '15', 'group': 'Zaplet regionalne analgezije', deprecated: true}


    {'name': 'več poskusov (>2)', 'id': '30', 'group': 'Zaplet regionalne analgezije'}
    {'name': 'neuspeh pri uvajanju', 'id': '31', 'group': 'Zaplet regionalne analgezije'}
    {'name': 'brez olajšanja bolečine', 'id': '32', 'group': 'Zaplet regionalne analgezije'}
    {'name': 'enostranska / asimetrična analgezija', 'id': '33', 'group': 'Zaplet regionalne analgezije'}
    {'name': 'ponovitev bolečine po uspešni analgeziji', 'id': '34', 'group': 'Zaplet regionalne analgezije'}
    {'name': 'nenamerna punkcija dure', 'id':'11', 'group': 'Zaplet regionalne analgezije'}
    {'name': 'postpunkcijski glavobol', 'id':'12', 'group': 'Zaplet regionalne analgezije'}
    {'name': 'postpunkcijski hematom', 'id': '35', 'group': 'Zaplet regionalne analgezije'}
    {'name': 'izveden epiduralni "blood-patch"', 'id':'36', 'group': 'Zaplet regionalne analgezije'}
    {'name': 'iztekanje krvi pri uvajanju', 'id':'37', 'group': 'Zaplet regionalne analgezije'}
    {'name': 'bolečina v hrbtu - zgodnja', 'id':'38', 'group': 'Zaplet regionalne analgezije'}
    {'name': 'bolečina v hrbtu – poznejša', 'id':'39', 'group': 'Zaplet regionalne analgezije'}
    {'name': 'parestezije pri vbodu', 'id':'40', 'group': 'Zaplet regionalne analgezije'}
    {'name': 'toksična reakcija na LA - blaga', 'id':'41', 'group': 'Zaplet regionalne analgezije'}
    {'name': 'toksična reakcija na LA - resna', 'id':'42', 'group': 'Zaplet regionalne analgezije'}
    {'name': 'prekomerna blokada - pogosti bolusi', 'id':'43', 'group': 'Zaplet regionalne analgezije'}
    {'name': 'prekomerna blokada - subdur. blok', 'id':'44', 'group': 'Zaplet regionalne analgezije'}
    {'name': 'prekomerna blokada - visok spin / totalni blok', 'id':'45', 'group': 'Zaplet regionalne analgezije'}
    {'name': 'retenca urina', 'id':'46', 'group': 'Zaplet regionalne analgezije'}
    {'name': 'meningitis', 'id':'47', 'group': 'Zaplet regionalne analgezije'}
    {'name': 'nevrološka okvara', 'id':'48', 'group': 'Zaplet regionalne analgezije'}
    {'name': 'epiduralni absces', 'id':'49', 'group': 'Zaplet regionalne analgezije'}
    {'name': 'epiduralni hematom', 'id':'50', 'group': 'Zaplet regionalne analgezije'}
    {'name': 'drugo / redki zapleti – glej opis', 'id':'51', 'group': 'Zaplet regionalne analgezije'}


    {'name': 'deceleracije', 'id': '16', 'group': 'Zaplet pri plodu'}
    {'name': 'druge CTG anomalije', 'id': '27', 'group': 'Zaplet pri plodu'}
    {'name': 'reanimacija novorojenčka', 'id': '28', 'group': 'Zaplet pri plodu'}
    {'name': 'smrt ploda (kot posledica zapletov)', 'id': '17', 'group': 'Zaplet pri plodu'}
    {'name': 'nenapredovanje poroda kot posledica analgezije', 'id': '18', 'group': 'Zaplet pri plodu'}
    {'name': 'vpliv anestetika na otroka', 'id': '19', 'group': 'Zaplet pri plodu', deprecated: true}
    {'name': 'drugo - glej opis', 'id': '29', 'group': 'Zaplet pri plodu'}]


