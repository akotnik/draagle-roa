mod = angular.module 'roaApp.directives', []



mod.directive 'input', () ->
    require: '?ngModel'
    restrict: 'E'
    link: (scope, element, attrs, ngModel) ->
      if attrs.type == "date" && ngModel
        element.bind 'change', () ->
          scope.$apply () -> ngModel.$setViewValue element.val()
