#google.setOnLoadCallback () -> undefined
#google.load 'visualization', '1', {packages: ['corechart']}

_ = window._


app = angular.module('roaApp', [
  'roaApp.services',
  'roaApp.controllers',
  'roaApp.directives',
  'googlechart',
  'ui.bootstrap',
  'ngRoute'
])


.config(['$routeProvider', ($routeProvider)  ->
  $routeProvider.
    when('/births/', {templateUrl: 'views/birth-index.html',   controller: 'BirthIndexCtrl'}).
    when('/birth/new/', {templateUrl: 'views/birth-detail.html',   controller: 'BirthEditCtrl'}).
    when('/birth/:birthId/', {templateUrl: 'views/birth-detail.html', controller: 'BirthEditCtrl'}).
    when('/stat/', {templateUrl: 'views/stat.html', controller: 'StatCtrl'}).
    when('/user/register', {templateUrl: 'views/user-register.html', controller: 'UserRegisterCtrl'}).
    when('/user/login/', {templateUrl: 'views/user-login.html'}).
    when('/export/', {templateUrl: 'views/export.html'}).
    otherwise({redirectTo: '/births/'})
])
    
.filter('idToName', ['Utils',  (Utils) -> Utils.idToName])

app.run(['$rootScope', '$location', 'Profile', 'DOMAIN', ($rootScope, $location, Profile, DOMAIN) ->
 profile = Profile.query()
 profile.$get () ->
   if profile.is_anonymous
     window.location = "/#!/profile/login/"
   #else if (profile.authorized_apps || []).indexOf('roa') == -1
   #    $location.path("/user/register/")
   #() -> undefined

 $rootScope.profile = profile
 $rootScope.domain = DOMAIN
 ])



