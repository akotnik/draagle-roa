_ = window._
mod = angular.module 'roaApp.controllers', []

mod.controller 'MainCtrl', [
  '$scope', '$location', 'MAX_ENTRY_AGE', ($scope, $location, MAX_ENTRY_AGE) ->
    $scope.is_birth_readonly = (birth) ->
      if !$scope.profile
        undefined
      entry_age = new Date() - Date.parse birth.created
      is_my = birth.user_id == $scope.profile.user_id
      !(is_my && entry_age < MAX_ENTRY_AGE)

    $scope.isPath = (path) ->
      $location.path().indexOf(path) == 0
]

mod.controller 'UserRegisterCtrl', [
  '$scope', '$location', '$timeout', 'Profile',
  ($scope, $location, $timeout, Profile) ->
    $scope.activate = () ->
      Profile.activate {code: $scope.code}, (response) ->
        if response.status == 0
          $location.path "/roahome/index.html"
  ]

mod.controller 'BirthIndexCtrl', [
  '$scope', '$location', '$routeParams', '$rootScope', 'Birth',
  ($scope, $location, $routeParams, $rootScope, Birth) ->
    allowReload = false
    $scope.page_size_options =
      '5': '5 na stran'
      '10':'10 na stran'
      '25': '25 na stran'
      '50': '50 na stran'
      '100': '100 na stran'

    $scope.sortables =
      "birth_date-2": "Datum poroda (nedavni)"
      "birth_date-1": "Datum poroda (starejši)"
      "hospital-1": "Porodnišnica A-Z"
      "hospital-2": "Porodnišnica Z-A"
      "doctor_id-1": "Anesteziolog A-Z"
      "doctor_id-2": "Anesteziolog Z-A"

    $rootScope.indexBookmark = $location.url()

    $scope.paging =
      page: parseInt($routeParams.page || "1")
      sort_by: $routeParams.sort_by || 'birth_date-2'
      show_all: $routeParams.show_all && true || false
      page_size: $routeParams.page_size || "10"

    $scope.births = Birth.query $scope.paging, (data) ->

      $scope.births = data.births
      $scope.records = data.count
      $scope.pages = Math.ceil(data.count / parseInt($scope.paging.page_size))
      allowReload = true

    reload = () ->
      allowReload = false
      console.log($scope.paging)
      $location.search $scope.paging

    $scope.$watch 'paging.page', reload
    $scope.$watch 'paging.page_size', reload
    $scope.$watch 'paging.show_all', reload
    $scope.$watch 'paging.sort_by', reload
  ]

mod.controller 'BirthEditCtrl', [
    '$scope', '$location', '$routeParams', 'Birth', 'BIRTH_MASTER',
    ($scope, $location, $routeParams, Birth, BIRTH_MASTER) ->

      $scope.analgesia_to_add = "?"
      $scope.readonly = false

      $scope.back = $scope.indexBookmark || '/births/'

      if $routeParams.birthId
        Birth.get {'birthId':$routeParams.birthId}, (data) ->
          #$scope.readonly = $scope.birth.user_id != $scope.profile.user_id
          $scope.birth = data
          $scope.readonly = false
      else
        $scope.birth = new Birth angular.extend({}, BIRTH_MASTER, $location.search())


      $scope.togglePreparation = (preparation_id) ->
        arr = $scope.birth.patient_preparation
        preparation_index = _.indexOf arr, preparation_id
        if preparation_index == -1
          arr.push preparation_id
        else
          arr.splice preparation_index, 1


      addAnalgesia = () ->
        value = $scope.analgesia_to_add
        if value && value != '?'
          $scope.birth.analgesias = $scope.birth.analgesias || []
          $scope.birth.analgesias.push
            'id': value,
            'complications': []

        $scope.analgesia_to_add = '?'
      

      $scope.$watch "analgesia_to_add != '?'", addAnalgesia

      $scope.isPreparation = (id) ->
        $scope.birth && ($scope.birth.patient_preparation || []).indexOf(id) != -1
      

      $scope.update = (addNew) ->
        if $scope.form.$valid
          $scope.birth.$save (response) ->
            if addNew
              birth = response.birth
              $location.path("/birth/new/")
              $location.search
                doctor_id: birth.doctor_id,
                hospital: birth.hospital,
                birth_date: birth.birth_date
              
              $location.replace()
            else
              $scope.birth = new Birth(response.birth)
              $scope.message_class = 'alert-success'
              $scope.message = "Shranjeno!"
            
        else
          $scope.message = "Popravi napačne vnose!"
          $scope.message_class = 'alert-error'
  ]

mod.controller 'AnalgesiaCtrl', ['$scope', 'DOMAIN', ($scope, DOMAIN) ->
  $scope.complication_to_add = '?'

  $scope.selectComplication = (group) ->
    DOMAIN.COMPLICATION.filter(
      (complication) -> complication.group == group && !complication.deprecated)
      .sort((a,b) -> if a.name >= b.name then 1 else -1)

  addComplication = () ->
    value = $scope.complication_to_add
    if value && value != '?'
      $scope.analgesia.complications.push value
    
    $scope.complication_to_add = '?'

  $scope.removeAnalgesia = (analgesias, analgesia) ->
    analgesias.splice(analgesias.indexOf(analgesia), 1)

  $scope.removeComplication = (analgesia, complication) ->
    c = analgesia.complications
    c.splice c.indexOf(complication), 1

  $scope.$watch "complication_to_add != '?'", addComplication

]

###
  http://stackoverflow.com/questions/14375728/angularjs-and-google-charts
###

mod.controller 'StatCtrl', [
  '$scope', 'Stat', 'Utils', '$routeParams', '$location'
  ($scope, Stat, Utils, $routeParams, $location) ->
    $scope.hospital = $routeParams.hospital
    $scope.title = "Uporaba analgezij po porodnišnicah"
    $scope.chartTypes = [
      {id: 'PieChart', name: 'Tortni diagram'}
      {id: 'BarChart', name: 'Stolpični diagram'}
      {id: 'Table', name: 'Tabela'}
      ]
    $scope.chart =
      "type": $routeParams.chartType || 'PieChart'
      "displayed": true
      "cssStyle": "height:600px; width:100%;"
      "data":
        "cols": [
          {id: 'analgesia', label: 'analgesia', type:'string'}
          {id: 'count', label: 'števec', type:'number'}
        ],
        "rows": [ ]
      
      "options":
        "title": ""
        "isStacked": "true"
        "fill": 20
        "selectVisibilityTreshold": 0
        "displayExactValues": true
        "vAxis":
          "title": "analgezija"
          "gridlines":
            "count": 10
        "hAxis":
          "title": "delež"

    Stat.query (response) ->
      hospital_ids = _.uniq (_.map response.data, (o) -> o[0])
      sum = (a) -> _.reduce(a, 
        (s, o) -> s + parseInt(o)
        0)
      $scope.hospitals = _.map hospital_ids, (o) ->
        id: o
        name: Utils.idToName o, $scope.domain.HOSPITAL
      if $scope.hospital
        data = _.filter response.data, (o) -> o[0] == $scope.hospital
        $scope.title = "Uporaba analgezij v porodnišnici " +
          Utils.idToName $scope.hospital, $scope.domain.HOSPITAL
      else
        groupped_by_hospital = _.groupBy(response.data, (o) -> o[1])
        data = _.map groupped_by_hospital, (val, key) ->
          [ 0, key, sum( _.map(val, _.last))]
        $scope.title = "Uporaba analegeizj vseh prodonišnicah skupaj"

      d = $scope.chart.data

      d.rows = _.map data, (l) ->
        c: [
          {v: Utils.idToName(l[1], $scope.domain.ANALGESIA) },
          {v: parseInt(l[2])}
        ]

      $scope.$watch 'hospital + chart.type', () ->
        $location.search
          hospital: if $scope.hospital then $scope.hospital else ''
          chartType: $scope.chart.type

]

mod.controller 'TestCtrl', ($scope) ->
  dump($scope)
  $scope.name = "Ales"
  dump($scope)
