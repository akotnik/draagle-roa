#!/bin/sh

# inspired by http://toastdriven.com/blog/2009/oct/09/scripting-tmux/

tmux start-server
tmux new-session -d -s roa -n roa
tmux select-window -t roa:0
tmux split-window -h 
tmux send-keys 'cd ~/prj/draagle/draagle-server; source envir; ./runserver.sh' 'C-m'
tmux split-window -v 
