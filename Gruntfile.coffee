proxySnippet = require('grunt-connect-proxy/lib/utils').proxyRequest
mountFolder =  (connect, dir) ->
  return connect.static(require('path').resolve(dir))


module.exports = (grunt) ->
  GIT_TAG = grunt.option('tag') || 'no-tag-specified'

  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks)

  # configurable paths
  yeomanConfig =
    app: 'app',
    dist: 'dist'
    tmp: '.tmp'
    bower: 'bower_components'

  grunt.initConfig
    yeoman: yeomanConfig

    # --
    watch:
      options:
        livereload: true

      coffee:
        files: ['<%= yeoman.app %>/scripts/{,*/}*.coffee']
        tasks: ['coffee:dist']

      coffeeTest:
        files: ['test/spec/{,*/}*.coffee'],
        tasks: ['coffee:test']

      app:
        files: [
          '<%= yeoman.app %>/{,*/}*.html',
          '{<%=yeoman.tmp%>,<%= yeoman.app %>}/styles/{,*/}*.css',
          '{<%=yeoman.tmp%>,<%= yeoman.app %>}/scripts/{,*/}*.js',
          '<%= yeoman.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
        ],
        tasks: []

    # --
    connect:
      options:
        port: 9000
        # Change this to '0.0.0.0' to access the server from outside.
        hostname: 'localhost'

      proxies: [
        {
          context: '/roa',
          host: 'si.draagle.com',
          port: 80,
          https: false,
          changeOrigin: true
        },
        {
          context: '/profile',
          host: 'si.draagle.com',
          port: 80,
          https: false,
          changeOrigin: true
        }

      ],

      livereload:
        options:
          middleware: (connect) ->
            return [
              proxySnippet
              mountFolder(connect, yeomanConfig.tmp)
              mountFolder(connect, yeomanConfig.app)
              mountFolder(connect, yeomanConfig.bower)
            ]
      test:
        options:
          middleware: (connect) ->
            return [
              mountFolder(connect, yeomanConfig.tmp),
              mountFolder(connect, 'test')
            ]
      dist:
        options:
          base: "<% yeoman.dist %>",
          keepalive: true,
          middleware: (connect) ->
            return [
              mountFolder(connect, 'dist')
            ]
    open:
      server:
        url: 'http://localhost:<%= connect.options.port %>'
    clean:
      dist:
        files: [{
          dot: true,
          src: [
            '<%=yeoman.tmp%>',
            '<%= yeoman.dist %>/*',
            '!<%= yeoman.dist %>/.git*'
          ]
        }]
      server: '<%=yeoman.tmp%>'
    jshint:
      options:
        jshintrc: '.jshintrc'
      all: [
        'Gruntfile.js',
        '<%= yeoman.app %>/scripts/{,*/}*.js'
      ]
    karma:
      unit:
        configFile: 'karma.conf.js',
        singleRun: true
    coffee:
      dist:
        files: [{
          expand: true,
          cwd: '<%= yeoman.app %>/scripts',
          src: '{,*/}*.coffee',
          dest: '<%=yeoman.tmp%>/scripts',
          ext: '.js'
        }]
      test:
        files: [{
          expand: true,
          cwd: 'test/spec',
          src: '{,*/}*.coffee',
          dest: '<%=yeoman.tmp%>/spec',
          ext: '.js'
        }]
    concat:
      dist:
        files:
          'dist/scripts/scripts.js': [
            '<%=yeoman.tmp%>/scripts/{,*/}*.js',
            '<%= yeoman.app %>/scripts/{,*/}*.js'
          ]
    useminPrepare:
      html: '<%= yeoman.app %>/index.html'
      options:
        dest: '<%= yeoman.dist %>'
    usemin:
      html: ['<%= yeoman.dist %>/{,*/}*.html']
      css: ['<%= yeoman.dist %>/styles/{,*/}*.css']
      options:
        dirs: ['<%= yeoman.dist %>']
    imagemin:
      dist:
        files: [{
          expand: true,
          cwd: '<%= yeoman.app %>/images',
          src: '{,*/}*.{png,jpg,jpeg}',
          dest: '<%= yeoman.dist %>/images'
        }]
    cssmin:
      dist:
        files:
          '<%= yeoman.dist %>/styles/main.css': [
            '<%=yeoman.tmp%>/styles/{,*/}*.css',
            '<%= yeoman.app %>/styles/{,*/}*.css'
          ]
    htmlmin:
      dist:
        options:
          ##removeCommentsFromCDATA: true,
          ##https://github.com/yeoman/grunt-usemin/issues/44
          collapseWhitespace: true,
          ##collapseBooleanAttributes: true,
          ##removeAttributeQuotes: true,
          ##removeRedundantAttributes: true,
          ##useShortDoctype: true,
          ##removeEmptyAttributes: true,
          ##removeOptionalTags: true
        files: [{
          expand: true,
          cwd: '<%= yeoman.app %>',
          src: ['*.html', 'views/*.html'],
          dest: '<%= yeoman.dist %>'
        }]
    cdnify:
      dist:
        html: ['<%= yeoman.dist %>/*.html']
    ngmin:
      dist:
        files: [{
          expand: true,
          cwd: '<%= yeoman.dist %>/scripts',
          src: '*.js',
          dest: '<%= yeoman.dist %>/scripts'
        }]
    rev:
      dist:
        files:
          src: [
            '<%= yeoman.dist %>/scripts/{,*/}*.js',
            '<%= yeoman.dist %>/styles/{,*/}*.css',
            '<%= yeoman.dist %>/img/{,*/}*.{png,jpg,jpeg,gif,webp,svg}',
            '<%= yeoman.dist %>/styles/fonts/*'
          ]
    copy:
      dist:
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= yeoman.app %>',
          dest: '<%= yeoman.dist %>',
          src: [
            '*.{ico,txt}',
            '.htaccess',
            '*.html',
            #'components/**/*',
            'img/{,*/}*.{gif,webp}',
            'styles/fonts/*'
          ]
        },
        {expand: true, cwd: 'app/scripts', src: 'i18n/angular-locale_sl-si.js', dest: '<%=yeoman.dist%>/scripts'},
        {expand: true, cwd: 'bower_components/bootstrap', src: 'img/*',  dest: '<%=yeoman.dist%>'}
        ]
    compress:
      dist:
        options:
          mode: 'gzip'
        files: [
          {src: ['dist/scripts/*.js'],   dest: 'dist/scripts/', expand: true, flatten: true   },
          {src: ['dist/styles/*.css'], dest: 'dist/styles/', expand: true, flatten: true   },
          {src: ['dist/views/*.html'], dest: 'dist/views/', expand: true, flatten: true   }
        ]
      package:
        options:
          archive: "dist-"+GIT_TAG+".zip"
        files: [
          {cwd: "dist", src: ['**'], dest: '', expand: true },
        ]

  grunt.registerTask 'server', [
    'clean:server',
    'coffee:dist',
    'configureProxies',
    'connect:livereload',
    'open',
    'watch'
    ]

  grunt.registerTask 'test', [
    'clean:server',
    'coffee',
    'connect:test',
    'karma'
    ]

  grunt.registerTask 'build', [
    'clean:dist',
    #'test',
    'coffee',
    #'jshint',
    'useminPrepare',
    'imagemin',
    'cssmin',
    'htmlmin',
    'concat',
    'copy',
    'ngmin',
    'uglify',
    'rev',
    'usemin',
    'compress:dist',
    'compress:package'
    ]

  grunt.registerTask 'default', ['build']
