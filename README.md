# Implementation of ROA with AngularJS

## Install

    npm install
    bower install

## Development

To start development server, type:

    grunt server

## Prepare environment

This describes setting up local working environment using nginx and django.
Default configuration routes api calls to front.draagle.com via proxy.

Copy `roa.dev` to `/etc/nginx/sites-available/` and make symbolic link to
`/etx/nginx/sites-enabled`.

Put `127.0.0.1 roa.dev` to `/etc/hosts`.

Run django on port 8000 and jekyll on port 4000.

### Mac OSX

Reload nginx with:

    sudo launchctl unload -w /Library/LaunchDaemons/homebrew.mxcl.nginx.plist
    sudo launchctl load -w /Library/LaunchDaemons/homebrew.mxcl.nginx.plist

