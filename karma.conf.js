module.exports = function (config) {
  config.set({
    basePath: '',

    files: [
      'bower_components/angular/angular.js',
      'bower_components/angular-resource/angular-resource.js',
      'bower_components/angular-route/angular-route.js',
      'bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
      'bower_components/angular-mocks/angular-*.js',
      'bower_components/angular-google-chart/ng-google-chart.js',
      'app/scripts/**/*.js',
      'app/scripts/**/*.coffee',
      'test/unit/**/*.coffee'
    ],

    frameworks: ['jasmine'],

    autoWatch: true,

    browsers: ['Chrome'],

    junitReporter: {
      outputFile: 'test_out/unit.xml',
      suite: 'unit'
    }
  });
};
